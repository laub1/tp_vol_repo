package sopra.formation.vol.model;

import java.util.ArrayList;

public class VoyageVol {

	private int ordre;
	private Vol vol;
	private ArrayList<Voyage> lesVoyages = new ArrayList<Voyage>();

	

	public int getOrdre() {
		return ordre;
	}

	public void setOrdre(int ordre) {
		this.ordre = ordre;
	}

	public Vol getVol() {
		return vol;
	}

	public void setVol(Vol vol) {
		this.vol = vol;
	}

	public ArrayList<Voyage> getLesVoyages() {
		return lesVoyages;
	}

	public void setLesVoyages(ArrayList<Voyage> lesVoyages) {
		this.lesVoyages = lesVoyages;
	}
	

}
