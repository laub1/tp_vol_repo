package sopra.formation.vol.model;

import java.util.ArrayList;
import java.util.Date;

public class Vol {
	public Vol() {
	}
	
	private Date dateDepart;
	private int numero;
	private Date dateArrivee;
	private Boolean ouvert;
	private int nbPlaces;
	private Aeroport aeroportDepart;
	private Aeroport aeroportArrivee;
	private Compagnie compagnie;
	private ArrayList<VoyageVol> lesVoyagesVols = new ArrayList<VoyageVol>();


	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public Date getDateArrivee() {
		return dateArrivee;
	}
	public void setDateArrivee(Date dateArrivee) {
		this.dateArrivee = dateArrivee;
	}
	public Boolean getOuvert() {
		return ouvert;
	}
	public void setOuvert(Boolean ouvert) {
		this.ouvert = ouvert;
	}
	public int getNbPlaces() {
		return nbPlaces;
	}
	public void setNbPlaces(int nbPlaces) {
		this.nbPlaces = nbPlaces;
	}
	public Date getDateDepart() {
		return dateDepart;
	}
	public void setDateDepart(Date dateDepart) {
		this.dateDepart = dateDepart;
	}
	public Compagnie getCompagnie() {
		return compagnie;
	}
	public void setCompagnie(Compagnie compagnie) {
		this.compagnie = compagnie;
	}
	public ArrayList<VoyageVol> getLesVoyagesVols() {
		return lesVoyagesVols;
	}
	public void setLesVoyagesVols(ArrayList<VoyageVol> lesVoyagesVols) {
		this.lesVoyagesVols = lesVoyagesVols;
	}
	public Aeroport getAeroportDepart() {
		return aeroportDepart;
	}
	public void setAeroportDepart(Aeroport aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}
	public Aeroport getAeroportArrivee() {
		return aeroportArrivee;
	}
	public void setAeroportArrivee(Aeroport aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}

	
	
	
	
}
