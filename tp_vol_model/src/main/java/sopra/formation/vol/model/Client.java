package sopra.formation.vol.model;

import java.util.ArrayList;

public abstract class Client extends Personne {

	public Client() {
	}
		private String moyenPaiement;
		private ArrayList<Reservation> lesReservations= new ArrayList<Reservation>();
		private Adresse adresseFacturation;
		
		

	public String getMoyenPaiement() {
		return moyenPaiement;
	}

	public void setMoyenPaiement(String moyenPaiement) {
		this.moyenPaiement = moyenPaiement;
	}

	public ArrayList<Reservation> getLesReservations() {
		return lesReservations;
	}

	public void setLesReservations(ArrayList<Reservation> lesReservations) {
		this.lesReservations = lesReservations;
	}

	public Adresse getAdresseFacturation() {
		return adresseFacturation;
	}

	public void setAdresseFacturation(Adresse adresseFacturation) {
		this.adresseFacturation = adresseFacturation;
	}


}
