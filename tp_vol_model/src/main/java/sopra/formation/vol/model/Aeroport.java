package sopra.formation.vol.model;

import java.util.ArrayList;

public class Aeroport {
	public Aeroport() {
		
	}
	public Aeroport(String aeroportDepart, String aeroportArrivee) {
		super();
		this.aeroportDepart = aeroportDepart;
		this.aeroportArrivee = aeroportArrivee;
	}
	private String code;
	private String aeroportDepart;
	private String aeroportArrivee;
	private ArrayList<Ville> lesVilles = new ArrayList<Ville>();
	private ArrayList<Vol> lesVols = new ArrayList<Vol>();


	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getAeroportDepart() {
		return aeroportDepart;
	}

	public void setAeroportDepart(String aeroportDepart) {
		this.aeroportDepart = aeroportDepart;
	}

	public String getAeroportArrivee() {
		return aeroportArrivee;
	}

	public void setAeroportArrivee(String aeroportArrivee) {
		this.aeroportArrivee = aeroportArrivee;
	}

	public ArrayList<Ville> getLesVilles() {
		return lesVilles;
	}

	public void setLesVilles(ArrayList<Ville> lesVilles) {
		this.lesVilles = lesVilles;
	}

	public ArrayList<Vol> getLesVols() {
		return lesVols;
	}

	public void setLesVols(ArrayList<Vol> lesVols) {
		this.lesVols = lesVols;
	}


	
	

}
