package sopra.formation.vol.model;

import java.util.ArrayList;

public class Voyage {
	public Voyage() {
	}
	

	private VoyageVol voyagevol;
	private ArrayList<Reservation> lesReservations = new ArrayList<Reservation>();

	
	public VoyageVol getVoyagevol() {
		return voyagevol;
	}

	public void setVoyagevol(VoyageVol voyagevol) {
		this.voyagevol = voyagevol;
	}

	public ArrayList<Reservation> getLesReservations() {
		return lesReservations;
	}

	public void setLesReservations(ArrayList<Reservation> lesReservations) {
		this.lesReservations = lesReservations;
	}

	
}
