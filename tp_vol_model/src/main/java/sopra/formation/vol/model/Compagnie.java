package sopra.formation.vol.model;

import java.util.ArrayList;

public class Compagnie {
	public Compagnie() {
		
	}
	
	private String nomCompagnie;
	private ArrayList<Vol> lesVols = new ArrayList<Vol>();

	

	public String getNomCompagnie() {
		return nomCompagnie;
	}

	public void setNomCompagnie(String nomCompagnie) {
		this.nomCompagnie = nomCompagnie;
	}

	public ArrayList<Vol> getLesVols() {
		return lesVols;
	}

	public void setLesVols(ArrayList<Vol> lesVols) {
		this.lesVols = lesVols;
	}
	

	
}
