package sopra.formation.vol.model;

import java.util.Date;

public class Reservation {
	public Reservation() {
		
	}
	private String numero;
	private Boolean statut;
	private Float tarif;
	private Float tauxTVA;
	private Date dateReservation;
	private Voyage voyage;
	private Personne personne;
	private Client client;

	
	
	public String getNumero() {
		return numero;
	}
	public Voyage getVoyage() {
		return voyage;
	}
	public void setVoyage(Voyage voyage) {
		this.voyage = voyage;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Boolean getStatut() {
		return statut;
	}
	public void setStatut(Boolean statut) {
		this.statut = statut;
	}
	public Float getTarif() {
		return tarif;
	}
	public void setTarif(Float tarif) {
		this.tarif = tarif;
	}
	public Float getTauxTVA() {
		return tauxTVA;
	}
	public void setTauxTVA(Float tauxTVA) {
		this.tauxTVA = tauxTVA;
	}
	public Date getDateReservation() {
		return dateReservation;
	}
	public void setDateReservation(Date dateReservation) {
		this.dateReservation = dateReservation;
	}
	public Personne getPersonne() {
		return personne;
	}
	public void setPersonne(Personne personne) {
		this.personne = personne;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}

	
}
