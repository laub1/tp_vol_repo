package sopra.formation.vol.model;

import java.util.ArrayList;
import java.util.Date;

public class Passager extends Personne{
	public Passager() {
	}
	
	private String nom;
	private String prenoms;
	private String numIdentite;
	private String nationalite;
	private String civilite;
	private String typePI;
	private Date dtNaissance;
	private Date dateValiditePI;
	private ArrayList<Reservation> lesReservations= new ArrayList<Reservation>();

	
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenoms() {
		return prenoms;
	}
	public void setPrenoms(String prenoms) {
		this.prenoms = prenoms;
	}
	public String getNumIdentite() {
		return numIdentite;
	}
	public void setNumIdentite(String numIdentite) {
		this.numIdentite = numIdentite;
	}
	public String getNationalite() {
		return nationalite;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	public String getCivilite() {
		return civilite;
	}
	public void setCivilite(String civilite) {
		this.civilite = civilite;
	}
	public String getTypePI() {
		return typePI;
	}
	public void setTypePI(String typePI) {
		this.typePI = typePI;
	}
	public Date getDtNaissance() {
		return dtNaissance;
	}
	public void setDtNaissance(Date dtNaissance) {
		this.dtNaissance = dtNaissance;
	}
	public Date getDateValiditePI() {
		return dateValiditePI;
	}
	public void setDateValiditePI(Date dateValiditePI) {
		this.dateValiditePI = dateValiditePI;
	}
	public ArrayList<Reservation> getLesReservations() {
		return lesReservations;
	}
	public void setLesReservations(ArrayList<Reservation> lesReservations) {
		this.lesReservations = lesReservations;
	}
	
	
	
	

}
