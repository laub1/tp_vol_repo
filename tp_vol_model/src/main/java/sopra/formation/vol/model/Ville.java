package sopra.formation.vol.model;

import java.util.ArrayList;

public class Ville {
	public Ville() {
	}
	
	private String nom;
	private Aeroport aeroport;
	private ArrayList<Aeroport> lesAeroports = new ArrayList<Aeroport>();


	public Aeroport getAeroport() {
		return aeroport;
	}

	public void setAeroport(Aeroport aeroport) {
		this.aeroport = aeroport;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public void add(Ville merignac) {
		// TODO Auto-generated method stub
		
	}

	public ArrayList<Aeroport> getLesAeroports() {
		return lesAeroports;
	}

	public void setLesAeroports(ArrayList<Aeroport> lesAeroports) {
		this.lesAeroports = lesAeroports;
	}
	
	
}
