package sopra.formation.vol;

import java.util.Date;

import sopra.formation.vol.model.Adresse;
import sopra.formation.vol.model.Aeroport;
import sopra.formation.vol.model.ClientParticulier;
import sopra.formation.vol.model.ClientPro;
import sopra.formation.vol.model.Compagnie;
import sopra.formation.vol.model.Passager;
import sopra.formation.vol.model.Reservation;
import sopra.formation.vol.model.Ville;
import sopra.formation.vol.model.Vol;
import sopra.formation.vol.model.Voyage;
import sopra.formation.vol.model.VoyageVol;

public class Test {

	public static void main(String[] args) {
		// Creation des aeroports
		Aeroport parisCG = new Aeroport();
		parisCG.setCode("PCG");
		Aeroport ny = new Aeroport();
		ny.setCode("NY");
		ny.getAeroportArrivee();
		Aeroport lrcity = new Aeroport();
		lrcity.setCode("LR");
		lrcity.getAeroportDepart();
//		System.out.println("creation de l'aeroport " +lrcity.getCode() );
//		System.out.println("creation de l'aeroport " +parisCG.getCode() );
//		System.out.println("creation de l'aeroport " +ny.getCode() );

		
		//Creations des villes
		Ville laRochelle = new Ville();
		laRochelle.setNom("La Rochelle");
		Ville NewYork = new Ville();
		NewYork.setNom("New York");
		Ville paris = new Ville();
		paris.setNom("Paris");
		Ville fresnes = new Ville();
		fresnes.setNom("Fresnes");
		paris.getLesAeroports().add(parisCG);
		parisCG.getLesVilles().add(paris);
		parisCG.getLesVilles().add(fresnes);
//		System.out.println("creation de la ville de "+ laRochelle.getNom());
//		System.out.println("creation de la ville de "+ paris.getNom());
//		System.out.println("creation de la ville de "+ NewYork.getNom());

		// Creation des vols
		Vol lrParis = new Vol();
		lrParis.setAeroportDepart(lrcity);
		lrParis.setDateDepart(new Date(2019,6,1));
		lrParis.setNumero(256);
		lrParis.setDateArrivee(new Date(2019,6,1));
		lrParis.setOuvert(true);
		lrParis.setNbPlaces(200);
		Vol parisNY = new Vol();
		parisNY.getAeroportArrivee();
		parisNY.setDateDepart(new Date(2019,6,2));
		parisNY.setNumero(256);
		parisNY.setDateArrivee(new Date(2019,6,3));
		parisNY.setOuvert(true);
		parisNY.setNbPlaces(500);

		// Creation des Cie aerienne
		Compagnie airFrance = new Compagnie();
		lrParis.getCompagnie();
		airFrance.getLesVols().add(lrParis);
		parisNY.getCompagnie();
		airFrance.getLesVols().add(parisNY);
		airFrance.setNomCompagnie("Air France");
		Compagnie canadaAirways = new Compagnie();
		canadaAirways.getLesVols().add(parisNY);
		canadaAirways.setNomCompagnie("Canada Airways");
		System.out.println("creation de la compagnie aerienne "+ airFrance.getNomCompagnie());
		System.out.println("creation de la compagnie aerienne "+canadaAirways.getNomCompagnie());
		System.out.println("");


		// Creation des escales
		VoyageVol escale1 = new VoyageVol();
		escale1.getLesVoyages();
		escale1.setVol(lrParis);
		VoyageVol escale2 = new VoyageVol();
		escale2.setVol(parisNY);
		escale1.setOrdre(1);
		escale2.setOrdre(2);
		escale2.getLesVoyages();

		// creation du voyage
		Voyage lrny = new Voyage();
		lrny.setVoyagevol(escale1);

		// creation des reservations
		Reservation resa1 = new Reservation();
		resa1.setDateReservation(new Date(2019,5,28));
		resa1.setNumero("F32548");
		resa1.setStatut(true);
		resa1.setTarif((float) 1500);
		resa1.setTauxTVA((float) 20);
		resa1.getVoyage();
		lrny.getLesReservations().add(resa1);
		System.out.println("creation d'une reservation numero "+resa1.getNumero());		
		

		// Passager Leon Cam�
		Passager passager1 = new Passager();
		passager1.setCivilite("Monsieur");
		passager1.setNom("Cam�");
		passager1.setPrenoms("Leon");
		passager1.setDtNaissance(new Date (1957,3,27));
		passager1.setNumIdentite("FR2155458");
		passager1.setNationalite("Francais");
		passager1.setTypePI("Carte identite");
		passager1.setDateValiditePI(new Date (2020,1,15));
		passager1.getLesReservations().add(resa1);
		resa1.getPersonne();
		passager1.setTelephone("0546584023");
		passager1.setMail("cameleon@gmail.com");
		System.out.println("creation du passager "+ passager1.getCivilite()+ " "+ passager1.getNom()+ " "+passager1.getPrenoms());

		// Cr�ation client particulier
		ClientParticulier cltpart1 = new ClientParticulier();
		cltpart1.setCivilite("Madame");
		cltpart1.setNom("Rico");
		cltpart1.setPrenoms("Lea Gertrude");
		cltpart1.setMoyenPaiement("cash");
		System.out.println("creation du client particulier "+ cltpart1.getCivilite()+ " "+cltpart1.getPrenoms()+" "+ cltpart1.getNom() );

		// cr�ation client pro
		ClientPro cltpro1 = new ClientPro();
		cltpro1.setNumeroSIRET(3268203);
		cltpro1.setNomEntreprise("SOPRA STERIA");
		cltpro1.setNumTVA("201554");
		cltpro1.setTypeEntreprise("SARL");
		cltpro1.setMoyenPaiement("Virement bancaire");
		System.out.println("creation du client pro "+ cltpro1.getNomEntreprise() );


		// Adresse du client particulier
		Adresse adresse1 = new Adresse();
		adresse1.setVoie("7 allee des tennis");
		adresse1.setComplement("");
		adresse1.setCodePostale("17340");
		adresse1.setVille("Chatelaillon");
		adresse1.setPays("France");
		System.out.println("Adresse de "+cltpart1.getCivilite()+ " "+cltpart1.getPrenoms()+" "+ cltpart1.getNom()+" : " + adresse1.getVoie() +" "+adresse1.getComplement() +" "+ adresse1.getCodePostale() +" "+ adresse1.getVille());

	
		
		System.out.println("Reservation num�ro "+ resa1.getNumero()+" par le client "+cltpart1.getCivilite()+" "+cltpart1.getNom()+" "+cltpart1.getPrenoms()+" pour le passager " + passager1.getCivilite()+" "+passager1.getNom() + " "+passager1.getPrenoms()+ ". Total du :"+resa1.getTarif()+" euros");
		System.out.println("D�part de l'aeroport de "+ lrcity.getCode()+ " le "+lrParis.getDateDepart());
		
	}
	
	
}
